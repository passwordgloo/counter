# Counter

## English

### Purpose of use

*   classify personal  learning experience

*   left the footprint of learning

*   enlighten other's thoughts


### Contact information

- [jihulab](https://jihulab.com/passwordgloo)
- [Github](https://github.com/passwordgloo)
- [Gitee](https://gitee.com/passwordgloo)
- [Gmail](foresee.igloo@gmail.com)
- [163 mail](passwordgloo@163.com)

## 中文

### 项目用途

*   归档个人学习经验
*   留下自己学习印迹
*   启迪他人思想火花

### 联系信息

- [jihulab](https://jihulab.com/passwordgloo)
- [Github](https://github.com/passwordgloo)
- [Gitee](https://gitee.com/passwordgloo)
- [Gmail](foresee.igloo@gmail.com)
- [163邮箱](passwordgloo@163.com)

